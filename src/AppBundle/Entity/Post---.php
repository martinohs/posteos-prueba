<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 *
 * @ORM\Table(name="post", indexes={@ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class Post
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=500, nullable=false)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isImportant", type="boolean", nullable=false)
     */
    private $isimportant;

    /**
     * @var integer
     *
     * @ORM\Column(name="likes", type="smallint", nullable=false)
     */
    private $likes;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=16777215, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="blob", length=16777215, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="creator", type="string", length=70, nullable=false)
     */
    private $creator;

    /**
     * @var string
     *
     * @ORM\Column(name="from_area", type="string", length=100, nullable=true)
     */
    private $fromArea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="upload_date", type="date", nullable=true)
     */
    private $uploadDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

