<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 *
 * @ORM\Table(name="post", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="FK_post_media_content_noticia", columns={"media_img_id"})})
 * @ORM\Entity
 */
class Post
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=500, nullable=false)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isImportant", type="boolean", nullable=false)
     */
    private $isimportant;

    /**
     * @var integer
     *
     * @ORM\Column(name="likes", type="smallint", nullable=false)
     */
    private $likes;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=16777215, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="creator", type="string", length=70, nullable=false)
     */
    private $creator;

    /**
     * @var string
     *
     * @ORM\Column(name="from_area", type="string", length=100, nullable=true)
     */
    private $fromArea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="upload_date", type="date", nullable=true)
     */
    private $uploadDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\MediaContentNoticia
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\MediaContentNoticia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="media_img_id", referencedColumnName="meda_img_id")
     * })
     */
    private $mediaImg;

    
    
    
     /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set isimportant
     *
     * @param boolean $isimportant
     *
     * @return Post
     */
    public function setIsimportant($isimportant)
    {
        $this->isimportant = $isimportant;

        return $this;
    }

    /**
     * Get isimportant
     *
     * @return boolean
     */
    public function getIsimportant()
    {
        return $this->isimportant;
    }

    /**
     * Set likes
     *
     * @param integer $likes
     *
     * @return Post
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;

        return $this;
    }

    /**
     * Get likes
     *
     * @return integer
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set creator
     *
     * @param string $creator
     *
     * @return Post
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set upload_date
     *
     * @param \DateTime $upload_date
     *
     * @return Post
     */
    public function setUploadDate($uploadDate)
    {
        $this->uploadDate = $uploadDate;

        return $this;
    }

    /**
     * Get upload_date
     *
     * @return \DateTime
     */
    public function getUploadDate()
    {
        return $this->uploadDate;
    }

    /**
     * Set fromArea
     *
     * @param string $fromArea
     *
     * @return Post
     */
    public function setFromArea($fromArea)
    {
        $this->fromArea = $fromArea;

        return $this;
    }

    /**
     * Get fromArea
     *
     * @return string
     */
    public function getfromArea()
    {
        return $this->fromArea;
    }



    /**
     * Set mediaImg
     *
     * @param \AppBundle\Entity\MediaContentNoticia
     *
     * @return Post
     */
    public function setMediaImg($mediaImg)
    {
        $this->mediaImg = $mediaImg;

        return $this;
    }

    /**
     * Get mediaImg
     *
     * @return \AppBundle\Entity\MediaContentNoticia
     */
    public function getmediaImg()
    {
        return $this->mediaImg;
    }
    
}

