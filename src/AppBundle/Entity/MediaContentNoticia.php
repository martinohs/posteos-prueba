<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaContentNoticia
 *
 * @ORM\Table(name="media_content_noticia")
 * @ORM\Entity
 */
class MediaContentNoticia
{
    /**
     * @var string
     *
     * @ORM\Column(name="image_one", type="blob", length=16777215, nullable=true)
     */
    private $imageOne;

    /**
     * @var string
     *
     * @ORM\Column(name="image_two", type="blob", length=16777215, nullable=true)
     */
    private $imageTwo;

    /**
     * @var integer
     *
     * @ORM\Column(name="meda_img_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $medaImgId;
    
     /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imageOne
     *
     * @param string $imageOne
     *
     * @return Post
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;

        return $this;
    }

    /**
     * Get imageOne
     *
     * @return string
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * Set imageTwo
     *
     * @param string $imageTwo
     *
     * @return Post
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;

        return $this;
    }

    /**
     * Get imageTwo
     *
     * @return string
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }
}

