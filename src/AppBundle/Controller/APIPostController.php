<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Post;
use AppBundle\Entity\MediaContentNoticia;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\PropertyAccess\PropertyAccess;

class APIPostController extends Controller
{
    
    //Convierte un JSON en objeto tipo POST
    protected function deserializarPost(Request $request){
        
        //Inicializo el serializador
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
       
        //Obtengo los datos del request (json)
        $data = $request->getContent();
       
        //Creo el post a partir del deserialize
        $unPost = new Post();
        $unPost = $serializer->deserialize($data, Post::class, 'json');

        return $unPost;

    }


    /**
     * @Route("/api/addpost", methods={"POST"})
     */
    public function addPostAction(Request $request)
    {

        $unPost = new Post();
        $postImages = new MediaContentNoticia();

        $unPost = $this->deserializarPost($request);
      

        //Creo la fecha actual del POST
        $newDate = new \DateTime('now');
        $newDate->format('Y-m-d');

        $unPost->setUploadDate($newDate);
        $em = $this->getDoctrine()->getManager();
       
        $em->persist($unPost);
        $em->flush();

        $response = new JsonResponse(['msg'=>'Creado con exito']);
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/api/delpost/{postId}", methods={"DELETE"})
     */
     public function deletePostAction($postId){
        
        $postToDelete = $this->getDoctrine()->getRepository('AppBundle:Post')->findOneById($postId);
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($postToDelete);
        $em->flush();

        $response = new JsonResponse(['msg' => "Eliminado con exito"]);

        return $response;

     }

     /**
      * @Route("/api/modpost/{postId}", methods={"PUT"})
      */
     public function modPostAction(Request $request, $postId){
      //FALTAN AGREGAR COSAS!
        $postToMod = $this->getDoctrine()->getRepository('AppBundle:Post')->findOneById($postId);

        $oldPost = $this->deserializarPost($request);

        $postToMod->setContent($oldPost->getContent());
        $postToMod->setTitle($oldPost->getTitle());
        $postToMod->setImage($oldPost->getImage());
        $postToMod->setIsimportant($oldPost->getIsimportant());
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($postToMod);
        $em->flush();

        $response = new JsonResponse(['msg' => "Modificado con exito"]);
        return $response;
     }


      //  QUEDA COMO FUNCION AUXILIAR -> EN TODO CASO DESPUES ELIMINAR
     /**
      * @Route("/api/getpost/alltest", methods={"GET"})
      */

      public function getAllPostsActiontest(){
          
        $postsRepository = $this->getDoctrine()->getRepository('AppBundle:Post')->findAll();
        
        $postsArray = array();
            foreach($postsRepository as $unPost){
                
                $postArray_aux = array();
                $postArray_aux['id'] = $unPost->getId();
                $postArray_aux['title'] = $unPost->getTitle();
                $postArray_aux['content'] = $unPost->getContent();
                $postArray_aux['isImportant'] = $unPost->getIsimportant();
                $postArray_aux['likes'] = $unPost->getLikes();
                $postArray_aux['creator'] = $unPost->getCreator();
                $postArray_aux['fromArea'] = $unPost->getFromArea();
                $postArray_aux['uploadDate'] = $unPost->getUploadDate();
                $postArray_aux['mediaImg'] = $unPost->getmediaImg();
                $postsArray[]= $postArray_aux;

            }
        
            $response = new JsonResponse($postsArray);
            $response->headers->set('Access-Control-Allow-Origin', '*');
            return $response;

      }


      /**
      * @Route("/api/getpost/all", methods={"GET"})
      */

      public function getAllPostsAction(){
          
        $postsRepository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $query = $postsRepository->createQueryBuilder('p')
        ->orderBy('p.id', 'DESC')
        ->getQuery();
          
       $posts = $query->getResult();
        $postsArray = array();
            foreach($posts as $unPost){
                
                $postArray_aux = array();
                $postArray_aux['id'] = $unPost->getId();
                $postArray_aux['title'] = $unPost->getTitle();
                $postArray_aux['content'] = $unPost->getContent();
                $postArray_aux['isImportant'] = $unPost->getIsimportant();
                $postArray_aux['likes'] = $unPost->getLikes();
                $postArray_aux['creator'] = $unPost->getCreator();
                $postArray_aux['fromArea'] = $unPost->getFromArea();
                $postArray_aux['uploadDate'] = $unPost->getUploadDate();
                $postArray_aux['mediaImg'] = $unPost->getmediaImg();
                $postsArray[]= $postArray_aux;

            }
        
            $response = new JsonResponse($postsArray);
            $response->headers->set('Access-Control-Allow-Origin', '*');
            return $response;

      }
     








    /**
     * @Route("/api/likedpost/{postId}" , methods={"PUT"})
     */
      public function oneLikeAction($postId){
        
        $likedPost = $this->getDoctrine()->getRepository('AppBundle:Post')->findOneById($postId);

        $likes = $likedPost->getLikes();
        $likes++;

        $likedPost->setLikes($likes);

        $em = $this->getDoctrine()->getManager();
        $em->persist($likedPost);
        $em->flush();

        $response = new JsonResponse(['msg'=>'El post fue likeado con exito']);

        return $response;

      }

      /**
       * @Route("/api/getpost/{postId}", methods={"GET"})
       */
      public function getOnePostAction($postId){

        $unPost = $this->getDoctrine()->getRepository('AppBundle:Post')->findOneById($postId);

        $postArray = array();
        $postArray_aux['id'] = $unPost->getId();
        $postArray_aux['title'] = $unPost->getTitle();
        $postArray_aux['content'] = $unPost->getContent();
        $postArray_aux['isImportant'] = $unPost->getIsimportant();
        $postArray_aux['likes'] = $unPost->getLikes();
        $postArray_aux['creator'] = $unPost->getCreator();
        $postArray_aux['fromArea'] = $unPost->getFromArea();
        $postArray_aux['uploadDate'] = $unPost->getUploadDate();
        $postArray_aux['mediaImg'] = $unPost->getmediaImg();
        $postArray = $postArray_aux;
        
        $response = new JsonResponse([$postArray]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        
        return $response;


      }


}